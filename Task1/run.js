function getFlat10(){
    if (total_sum > 200)
        return total_sum-10
    return Infinity
}

function getBulk5(){
    sub_sum = 0
    for (const idx of [0, 1, 2]){
        if (quant[idx] > 10)
            sub_sum += 0.95*quant[idx]*prod_prices[idx]
    }
    return (sub_sum > 0) ? sub_sum : Infinity
}
function getBulk10(){
    if (total_units > 20)
        return total_sum*0.9
    return Infinity
}
function getTier50(){
    if (total_units <= 30)
        return Infinity

    sub_sum = 0
    for (const idx of [0, 1, 2]){
        prod_price = quant[idx]*prod_prices[idx]
        if (quant[idx] > 15)
            sub_sum += 0.5*prod_price
        else
            sub_sum += prod_price
    }
    return sub_sum
}

function getGiftCost(){
    sum = 0
    gifts.filter((val, idx)=>{
        if(val == "Y")
            sum+=quant[idx]
    });
    return sum;
}
function getShippingFee(){
    return (parseInt(total_units / 10) > 0) ? (parseInt(total_units / 10) * 5 + 5) : 5
}
function displayCart(){
    console.log("Product \t Price \t\tQuantity \tSubtotal")
    for (const i of [0, 1, 2]){
        console.log(String.fromCharCode(65+i), `\t\t\t \$${prod_prices[i]} \t\t\$${quant[i]} units \t \$${prod_prices[i]*quant[i]}`)
    }
    if(best_price < total_sum) {
        console.log(`'${discount_names[discount_idx]}' is applied\n\n`)
        console.log(`Discount \t-\$${total_sum-discounts[discount_idx]}\n\n`)
    }
    console.log(`Gift Wrap Fee \t \$${getGiftCost()}\n\n`)
    console.log(`Shipping Fee \t \$${getShippingFee()}\n\n`)
    console.log(`Grand Total \t \$${Math.round(getGiftCost()+getShippingFee()+best_price, 2)}`)
}

console.log("\nAvailable Products:-")
console.log("Product \t Price")

prod_prices = [20, 40, 50]
quant = []
gifts = []
discount_names = ["flat_10_discount", "bulk_5_discount", "bulk_10_discount", "tiered_50_discount"]
for (const i of [0, 1, 2]){
    console.log(`${String.fromCharCode(65+i)} \t\t \$${prod_prices[i]}`)}
console.log("\nEnter how many units you want to buy for")
for (const i of [0, 1, 2]){
    let unit = parseInt(prompt(`Units for ${String.fromCharCode(65+i)}`));
    quant.push(unit);
    console.log(`Product \$${String.fromCharCode(65+i)}:- \t\t${unit} units`);
    let gift = prompt("Wrap as a Gift?(Y/N)").toUpperCase();
    gifts.push(gift);
    console.log(`Wrap as a Gift?(Y/N):- \t${gift}`);
}
total_sum = prod_prices.reduce((x, y, idx) => x+y*quant[idx] );
total_units = quant.reduce((x, y) => x+y)

discounts = {0: getFlat10(), 1: getBulk5(), 2: getBulk10(), 3: getTier50()}
li=Object.entries(discounts)
discount_price = [total_sum, -1]
li.filter((val)=> {
    if (val[1] < discount_price[0]){
        discount_price[0]=val[1];
        discount_price[1]=parseInt(val[0]);
    }
});
discount_idx = discount_price[1]
best_price = discount_price[0]
console.log()
displayCart()
