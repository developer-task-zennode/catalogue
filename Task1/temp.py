def getFlat10():
    if total_sum > 200:
        return total_sum-10
    return float("inf")

def getBulk5():
    sub_sum = 0
    for idx in range(3):
        if quant[idx] > 10:
            sub_sum += 0.95*quant[idx]*prod_prices[idx]

    return sub_sum if sub_sum > 0 else float("inf")

def getBulk10():
    if total_units > 20:
        return total_sum*0.9
    return float("inf")

def getTier50():
    if total_units <= 30:
        return float("inf")
    
    sub_sum = 0
    for idx in range(3):
        prod_price = quant[idx]*prod_prices[idx]
        if quant[idx] > 15:
            sub_sum += 0.5*prod_price
        else:
            sub_sum += prod_price
    return sub_sum

def getGiftCost():
    return sum([quant[idx] for idx in range(3) if gifts[idx]=="Y"])

def getShippingFee():
    return ((total_units // 10) * 5 + 5) if (total_units // 10) > 0 else 5

def displayCart():
    print("Product \t Price \t\tQuantity \tSubtotal")
    for i in range(3):
        print(f"{chr(65+i)} \t\t ${prod_prices[i]} \t\t{quant[i]} units \t ${prod_prices[i]*quant[i]}")
    print(*["_"]*60, sep="", end="\n\n")
    if best_price < total_sum:
        print(f"'{discount_names[discount_idx]}' is applied", end="\n\n")
        print(f"Discount \t-${total_sum-discounts[discount_idx]}", end="\n\n")
    print(f"Gift Wrap Fee \t ${getGiftCost()}", end="\n\n")
    print(f"Shipping Fee \t ${getShippingFee()}", end="\n\n")
    print(f"Grand Total \t ${round(getGiftCost()+getShippingFee()+best_price, 2)}")



print("\nAvailable Products:-")
print("Product \t Price")

prod_prices = [20, 40, 50]
quant = []
gifts = []
discount_names = ["flat_10_discount", "bulk_5_discount", "bulk_10_discount", "tiered_50_discount"]

for i in range(3):
    print(f"{chr(65+i)} \t\t ${prod_prices[i]}")
print("\nEnter how many units you want to buy for")
for i in range(3):
    print(f"Product {chr(65+i)} \t\t")
    quant.append(int(input()))
    print("Wrap as a Gift?(Y/N)")
    gifts.append(input().upper())

total_sum = sum([prod_prices[i]*quant[i] for i in range(3)])
total_units = sum(quant)

discounts = {0: getFlat10(), 1: getBulk5(), 2: getBulk10(), 3: getTier50()}

discount_idx = min(discounts, key=discounts.get)
best_price = min(total_sum, discounts[discount_idx])
print()
displayCart()
