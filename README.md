## This is project is for Developer task 2024 for the role of 'Junior Software Developer' organized by Zennode.

### For Task 1
There are two files in this repo related to
1. JavaSript
2. Python

You can see the assignment [here](https://docs.google.com/document/d/16-JiaNH1qvQ-3OCd6wdR8TamYTO7r70e3UtEKKimN8M/edit)

#### - To execute JavaSript code, add the script in any browser and it'll work.

For example, open **inspect** mode on any webpage then go to **console** tab on the top and copy the code, run it.

#### - To execute Python script, just run it in any environment with Python installed.
